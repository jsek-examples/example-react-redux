import { myMiddleware } from '../src/app.middleware';
import { reducer } from '../src/app.reducer';
import { createStore, applyMiddleware } from 'redux';

describe('[Middleware]', () => {
    beforeEach(() => {
        fetch.resetMocks()
    })

    it('should fetch random value and pass it to the store', async () => {
        const newEvents = [];

        fetch.once('42');

        const fakeStore = {
            getState: () => ({ data: [1, 2] }),
            dispatch: (action) => { newEvents.push(action) }
        };
        const fakeNext = () => { };

        await myMiddleware(fakeStore)(fakeNext)({ type: 'FETCH_RANDOM' })

        expect(newEvents).toContainEqual({ type: 'ADD_VALUE', arg: { v: 42 } });
    })

    it('should call for new value with narrow min/max every time', async () => {
        const store = createStore(
            reducer,
            { data: [] },
            applyMiddleware(myMiddleware)
        );

        const parseUrl = ([url]) => {
            return [
                parseInt(/min=(\d+)/.exec(url)[1], 10),
                parseInt(/max=(\d+)/.exec(url)[1], 10)
            ]
        }

        fetch.mockResponse('42');

        await store.dispatch({ type: 'FETCH_RANDOM' });
        await store.dispatch({ type: 'FETCH_RANDOM' });
        await store.dispatch({ type: 'FETCH_RANDOM' });
        await store.dispatch({ type: 'FETCH_RANDOM' });

        expect(store.getState().data).toHaveLength(4);

        const fetchArgs = fetch.mock.calls.map(parseUrl);

        expect(fetchArgs).toMatchSnapshot();
    })
})