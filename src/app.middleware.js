import regression from 'regression';

export const myMiddleware = store => next => action => {
    next(action);
    switch (action.type) {
        case "FETCH_RANDOM": {
            store.dispatch({ type: "SET_LOADING", arg: true });
            store.dispatch({ type: "CLEAR_ERROR" });

            const dataLength = store.getState().data.length;
            const min = Math.round(Math.min(1900, 2000 * dataLength / 20));
            const max = 4000 - min;

            return fetch(
                `https://www.random.org/integers/?num=1&min=${min}&max=${max}&col=1&base=10&format=plain&rnd=new`,
                { credentials: "same-origin", mode: "cors" }
            )
                .then(response => response.json())
                .then(x => store.dispatch({ type: "ADD_VALUE", arg: { v: x } }))
                .finally(() => store.dispatch({ type: "SET_LOADING", arg: false }))
                .catch(() => store.dispatch({ type: "SET_ERROR" }));
        }
        case "ADD_VALUE": {
            const allPoints = store.getState().data.map((x, i) => [i, x.v]);
            const { points } = regression.linear(allPoints);
            store.dispatch({
                type: "SET_TREND",
                arg: points.map(x => x[1])
            })
            return Promise.resolve(points);
        }
    }
};
