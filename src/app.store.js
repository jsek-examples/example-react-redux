import { createStore, applyMiddleware, compose } from "redux";
import { devToolsEnhancer } from "redux-devtools-extension";
import { myMiddleware } from './app.middleware';
import { reducer } from './app.reducer';

const loggingMiddleware = storeAPI => next => action => {
  console.log("dispatching", action);
  let result = next(action);
  console.log("next state", storeAPI.getState());
  return result;
};

const initialData = [
    {v: 4000},
    {v: 3000},
    {v: 2000},
    {v: 2780},
    {v: 1890},
    {v: 2390},
    {v: 3490},
];

export const store = createStore(
  reducer,
  { data: initialData, initialData },
  compose(applyMiddleware(myMiddleware, loggingMiddleware), devToolsEnhancer({
    name: 'Demo',
    shouldHotReload: true,
    actionsBlacklist: ['SET_LOADING', 'CLEAR_ERROR']
  }))
);
