import React from "react";
import ReactDOM from "react-dom";
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { Spinner } from 'office-ui-fabric-react/lib/Spinner';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { LineChart, Line, XAxis, YAxis, CartesianGrid } from 'recharts';
import { store } from './app.store';
import './app.css';

const render = () => {
  const state = store.getState();
  return ReactDOM.render(
    <Fabric>
      <LineChart width={600} height={300} max={4000} data={state.data}>
        <Line type="monotone" dataKey="v" stroke="blue" />
        <Line type="monotone" dataKey="t" stroke="red" dot={false} isAnimationActive={false} />
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis/>
        <YAxis hide={true} domain={[0, 4000]}/>
      </LineChart>
      { state.error && <strong>{state.error}</strong> }
      <br/>
      <DefaultButton onClick={() => store.dispatch({ type: "FETCH_RANDOM" })} primary={true}>{ state.loading ? <Spinner /> : '+' }</DefaultButton>
      <DefaultButton onClick={() => store.dispatch({ type: "REMOVE_VALUE" })}>-</DefaultButton>
      <DefaultButton onClick={() => store.dispatch({ type: "CLEAR_DATA" })}>↺</DefaultButton>
    </Fabric>,
    document.getElementById("app")
  );
};

render();
store.subscribe(render);
