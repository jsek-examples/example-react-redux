export const reducer = (state, action) => {
    switch (action.type) {
        case "CLEAR_DATA"   : return { ...state, data: state.initialData };
        case "REMOVE_VALUE" : return { ...state, data: state.data.slice(0, state.data.length - 1) };
        case "ADD_VALUE"    : return { ...state, data: [...state.data.slice(state.data.length > 20 ? 1 : 0), action.arg] };
        case "SET_LOADING"  : return { ...state, loading: action.arg };
        case "SET_ERROR"    : return { ...state, error: "Something wrong happened" };
        case "SET_TREND"    : return { ...state, data: [...state.data.map((x, i) => ({...x, t: action.arg[i]}))] };
        case "CLEAR_ERROR"  : return { ...state, error: undefined };
        default: return state;
    }
};
