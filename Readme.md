# Example React + Redux

Example of React app with Redux and middleware

![](./screenshot.png)

# Setup

```sh
npm i
npm start
```